﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APIGraphQLOpenDoor.Entities
{
    public class DoorAccessLog
    {
        public int DoorAccessLogId { get; set; }
        public int DoorId { get; set; }
        public string UserRfId { get; set; }
        public string Reason { get; set; }
        public DateTime DateLog { get; set; }
    }
}
