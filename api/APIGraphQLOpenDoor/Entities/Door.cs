﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace APIGraphQLOpenDoor.Entities
{
    public class Door
    {
        public int DoorId { get; set; }

        [Required]
        [MaxLength(150)]
        public string Nome { get; set; }
    }
}