﻿using APIGraphQLOpenDoor.Interfaces;
using APIGraphQLOpenDoor.Entities;
using APIGraphQLOpenDoor.Context;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace APIGraphQLOpenDoor.Repositories
{
    public class DoorRepository : IDoorRepository
    {
        private readonly AppDbContext _context;
        public DoorRepository(AppDbContext context)
        {
            _context = context;
        }

        public Door Get(int doorId) => _context.Doors.Where(w => w.DoorId == doorId).FirstOrDefault();

        public async Task<DoorAcess> GetAcessPermission(int doorId)
        {
            var acessDoor = new DoorAcess();
            var door = Get(doorId);
            acessDoor.DoorId = doorId;
            acessDoor.Nome = door.Nome;
            var rfid = Arduino.ArduinoUtil.ReadRfId();

            var userPermission = _context.DoorPermissions.Where(w => w.DoorId == doorId && w.UserRfId == rfid).FirstOrDefault();

            if (userPermission != null)
            {
                acessDoor.Acess = true;
                acessDoor.ResultMsg = "Acesso permitido...";
                acessDoor.UserRfId = rfid;
            }
            else
            {
                acessDoor.Acess = false;
                acessDoor.ResultMsg = "Usuário sem permissão de acesso...";
                acessDoor.UserRfId = rfid;
                await Arduino.ArduinoUtil.CloseDoor();
            }
            return acessDoor;
        }

        public IEnumerable<Door> GetAll() => _context.Doors.ToList();

        public async Task<DoorAcess> GrantAcess(int doorId, string userRfId, string motivo)
        {
            var acessDoor = new DoorAcess();
            var door = Get(doorId);
            acessDoor.DoorId = doorId;
            acessDoor.Nome = door.Nome;

            await Arduino.ArduinoUtil.OpenDoor();

            var log = new DoorAccessLog()
            {
                DoorId = doorId,
                UserRfId = userRfId,
                DateLog = DateTime.Now,
                Reason = motivo
            };
            _context.DoorAccessLogs.Add(log);
            _context.SaveChanges();            
            return acessDoor;
        }
    }
}