﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace APIGraphQLOpenDoor.Migrations
{
    public partial class LogDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateLog",
                table: "DoorAccessLogs",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Reason",
                table: "DoorAccessLogs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateLog",
                table: "DoorAccessLogs");

            migrationBuilder.DropColumn(
                name: "Reason",
                table: "DoorAccessLogs");
        }
    }
}
