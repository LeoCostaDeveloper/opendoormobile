﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace APIGraphQLOpenDoor.Migrations
{
    public partial class Log : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DoorAccessLogs",
                columns: table => new
                {
                    DoorAccessLogId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DoorId = table.Column<int>(nullable: false),
                    UserRfId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoorAccessLogs", x => x.DoorAccessLogId);
                });

            migrationBuilder.CreateTable(
                name: "DoorPermissions",
                columns: table => new
                {
                    DoorPermissionId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DoorId = table.Column<int>(nullable: false),
                    UserRfId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DoorPermissions", x => x.DoorPermissionId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DoorAccessLogs");

            migrationBuilder.DropTable(
                name: "DoorPermissions");
        }
    }
}
