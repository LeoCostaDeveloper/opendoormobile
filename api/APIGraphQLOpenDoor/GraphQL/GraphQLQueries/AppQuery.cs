﻿using APIGraphQLOpenDoor.Interfaces;
using APIGraphQLOpenDoor.GraphQL.GraphQLTypes;
using GraphQL.Types;
using System.Collections.Generic;

namespace APIGraphQLOpenDoor.GraphQL.GraphQLQueries
{
    public class AppQuery : ObjectGraphType
    {
        public AppQuery(IDoorRepository repository)
        {
            Field<ListGraphType<DoorType>>(
               "doors",
               resolve: context => repository.GetAll()
           );

            Field<DoorType>(
             "door",
             arguments: new QueryArguments(new List<QueryArgument>
                 {
                     new QueryArgument<IdGraphType>
                     {
                         Name = "id"
                     }
                 }),
             resolve: context =>
             {
                 var doorId = context.GetArgument<int?>("id");
                 if (doorId.HasValue)
                 {
                     return repository.Get(doorId.Value);
                 }
                 return null;
             }
         );

            Field<DoorAcessType>(
             "doorAcessPermissiion",
             arguments: new QueryArguments(new List<QueryArgument>
                {
                    new QueryArgument<IdGraphType>
                    {
                        Name = "id"
                    }
                }),
             resolve: context =>
             {
                 var doorId = context.GetArgument<int?>("id");
                 if (doorId.HasValue)
                 {
                     return repository.GetAcessPermission(doorId.Value);
                 }
                 return null;
             }
         );

        Field<DoorAcessType>(
             "doorGrantAcess",
             arguments: new QueryArguments(new List<QueryArgument>
                {
                    new QueryArgument<IdGraphType>
                    {
                        Name = "id"
                    },
                    new QueryArgument<StringGraphType>
                    {
                        Name = "userRfId"
                    },
                    new QueryArgument<StringGraphType>
                    {
                        Name = "reason"
                    },
                }),
             resolve: context =>
             {
                 var doorId = context.GetArgument<int?>("id");
                 var userRfId = context.GetArgument<string>("userRfId");
                 var motivo = context.GetArgument<string>("reason");
                 if (doorId.HasValue && !string.IsNullOrEmpty(userRfId) && !string.IsNullOrEmpty(motivo))
                 {
                     return repository.GrantAcess(doorId.Value, userRfId, motivo);
                 }
                 return null;
             }
         );
        }
    }
}