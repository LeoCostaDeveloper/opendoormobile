﻿using APIGraphQLOpenDoor.Entities;
using GraphQL.Types;

namespace APIGraphQLOpenDoor.GraphQL.GraphQLTypes
{
    public class DoorType : ObjectGraphType<Door>
    {
        public DoorType()
        {
            Field(x => x.DoorId, type: typeof(IdGraphType))
                .Description("Propriedade DoorId do objeto Door.");
            Field(x => x.Nome)
                .Description("Propriedade Nome do objeto Door.");
        }
    }
}