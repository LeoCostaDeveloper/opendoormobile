﻿using APIGraphQLOpenDoor.Entities;
using GraphQL.Types;


namespace APIGraphQLOpenDoor.GraphQL.GraphQLTypes
{
    public class DoorAcessType : ObjectGraphType<DoorAcess>
    {
        public DoorAcessType()
        {
            Field(x => x.DoorId, type: typeof(IdGraphType))
                .Description("Propriedade DoorId do objeto DoorAcess.");
            Field(x => x.Nome)
                .Description("Propriedade Nome do objeto DoorAcess.");
            Field(x => x.Acess)
                .Description("Propriedade Acess do objeto DoorAcess.");
            Field(x => x.UserRfId)
                .Description("Propriedade UserRfId do objeto DoorAcess.");
            Field(x => x.ResultMsg)
                .Description("Propriedade ResultMsg do objeto DoorAcess.");
            Field(x => x.Motivo)
                .Description("Propriedade Motivo do objeto DoorAcess.");
        }
    }
}