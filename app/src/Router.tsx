import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { IonRouterOutlet } from '@ionic/react'
import { IonReactRouter } from '@ionic/react-router'

import DoorsListPage from './pages/DoorsListPage'
import OpenDoorPage from './pages/OpenDoorPage'
import AcessDoorsGrantPage from './pages/AcessDoorsGrantPage'

const Router: React.FC = () => (
  <IonReactRouter>
    <IonRouterOutlet>
      <Route path="/openDoors" exact>
        <DoorsListPage />
      </Route>
      <Route path="/openDoors/:id" exact>
        <OpenDoorPage />
      </Route>
      <Route path="/openDoors/:id/:userRfId/:reason" exact>
        <AcessDoorsGrantPage />
      </Route>
      <Route path="/" exact>
        <Redirect to="/openDoors" />
      </Route>
    </IonRouterOutlet>
  </IonReactRouter>
)

export default Router
