import React from 'react'
import {
  IonCard,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonCardContent,
  IonLoading,
} from '@ionic/react'

import styles from './Doors.module.scss'
import { useParams } from 'react-router'
import { useDoorGetQuery } from '../generated/graphql'

const AcessDoorsGrant: React.FC = () => {
  const { id } = useParams<{ id: string }>()
  const { data, loading } = useDoorGetQuery({
    variables: { id },
  })

  if (loading) {
    return <IonLoading isOpen={loading} message="Carregando..." />
  }

  return (
    <IonCard button className={styles.card}>
      <IonCardHeader>
        <IonCardSubtitle>Solicitação de Acesso</IonCardSubtitle>
        <IonCardTitle>{data && data!.door.nome}</IonCardTitle>
      </IonCardHeader>
      <IonCardContent>
        <p>Acesso Permitido</p>
      </IonCardContent>
    </IonCard>
  )
}

export default AcessDoorsGrant
