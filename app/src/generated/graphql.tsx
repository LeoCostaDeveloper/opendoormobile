/* eslint-disable */
import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';
export type Maybe<T> = T;

/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  /** 
 * The `Date` scalar type represents a year, month and day in accordance with the
   * [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.
 **/
  Date: any,
  /** 
 * The `DateTime` scalar type represents a date and time. `DateTime` expects
   * timestamps to be formatted in accordance with the
   * [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.
 **/
  DateTime: any,
  /** 
 * The `DateTimeOffset` scalar type represents a date, time and offset from UTC.
   * `DateTimeOffset` expects timestamps to be formatted in accordance with the
   * [ISO-8601](https://en.wikipedia.org/wiki/ISO_8601) standard.
 **/
  DateTimeOffset: any,
  Decimal: any,
  /** The `Milliseconds` scalar type represents a period of time represented as the total number of milliseconds. */
  Milliseconds: any,
  /** The `Seconds` scalar type represents a period of time represented as the total number of seconds. */
  Seconds: any,
};

export type AppQuery = {
   __typename?: 'AppQuery',
  door: Maybe<DoorType>,
  doorAcessPermissiion: Maybe<DoorAcessType>,
  doorGrantAcess: Maybe<DoorAcessType>,
  doors: Maybe<Array<Maybe<DoorType>>>,
};


export type AppQueryDoorArgs = {
  id?: Maybe<Scalars['ID']>
};


export type AppQueryDoorAcessPermissiionArgs = {
  id?: Maybe<Scalars['ID']>
};


export type AppQueryDoorGrantAcessArgs = {
  id?: Maybe<Scalars['ID']>,
  userRfId?: Maybe<Scalars['String']>,
  reason?: Maybe<Scalars['String']>
};





export type DoorAcessType = {
   __typename?: 'DoorAcessType',
  /** Propriedade Acess do objeto DoorAcess. */
  acess: Scalars['Boolean'],
  /** Propriedade DoorId do objeto DoorAcess. */
  doorId: Maybe<Scalars['ID']>,
  /** Propriedade Motivo do objeto DoorAcess. */
  motivo: Scalars['String'],
  /** Propriedade Nome do objeto DoorAcess. */
  nome: Scalars['String'],
  /** Propriedade ResultMsg do objeto DoorAcess. */
  resultMsg: Scalars['String'],
  /** Propriedade UserRfId do objeto DoorAcess. */
  userRfId: Scalars['String'],
};

export type DoorType = {
   __typename?: 'DoorType',
  /** Propriedade DoorId do objeto Door. */
  doorId: Maybe<Scalars['ID']>,
  /** Propriedade Nome do objeto Door. */
  nome: Scalars['String'],
};



export type DoorsGetAllQueryVariables = {};


export type DoorsGetAllQuery = (
  { __typename?: 'AppQuery' }
  & { doors: Maybe<Array<Maybe<(
    { __typename?: 'DoorType' }
    & Pick<DoorType, 'doorId' | 'nome'>
  )>>> }
);

export type DoorGetQueryVariables = {
  id: Scalars['ID']
};


export type DoorGetQuery = (
  { __typename?: 'AppQuery' }
  & { door: Maybe<(
    { __typename?: 'DoorType' }
    & Pick<DoorType, 'doorId' | 'nome'>
  )> }
);

export type DoorAcessPermissionQueryVariables = {
  id: Scalars['ID']
};


export type DoorAcessPermissionQuery = (
  { __typename?: 'AppQuery' }
  & { doorAcessPermissiion: Maybe<(
    { __typename?: 'DoorAcessType' }
    & Pick<DoorAcessType, 'acess' | 'doorId' | 'nome' | 'resultMsg' | 'userRfId'>
  )> }
);

export type DoorGrantAcessQueryVariables = {
  id: Scalars['ID'],
  userRfId: Maybe<Scalars['String']>,
  reason: Maybe<Scalars['String']>
};


export type DoorGrantAcessQuery = (
  { __typename?: 'AppQuery' }
  & { doorGrantAcess: Maybe<(
    { __typename?: 'DoorAcessType' }
    & Pick<DoorAcessType, 'acess' | 'doorId' | 'nome' | 'resultMsg' | 'userRfId' | 'motivo'>
  )> }
);


export const DoorsGetAllDocument = gql`
    query DoorsGetAll {
  doors {
    doorId
    nome
  }
}
    `;

/**
 * __useDoorsGetAllQuery__
 *
 * To run a query within a React component, call `useDoorsGetAllQuery` and pass it any options that fit your needs.
 * When your component renders, `useDoorsGetAllQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDoorsGetAllQuery({
 *   variables: {
 *   },
 * });
 */
export function useDoorsGetAllQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<DoorsGetAllQuery, DoorsGetAllQueryVariables>) {
        return ApolloReactHooks.useQuery<DoorsGetAllQuery, DoorsGetAllQueryVariables>(DoorsGetAllDocument, baseOptions);
      }
export function useDoorsGetAllLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<DoorsGetAllQuery, DoorsGetAllQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<DoorsGetAllQuery, DoorsGetAllQueryVariables>(DoorsGetAllDocument, baseOptions);
        }
export type DoorsGetAllQueryHookResult = ReturnType<typeof useDoorsGetAllQuery>;
export type DoorsGetAllLazyQueryHookResult = ReturnType<typeof useDoorsGetAllLazyQuery>;
export const DoorGetDocument = gql`
    query DoorGet($id: ID!) {
  door(id: $id) {
    doorId
    nome
  }
}
    `;

/**
 * __useDoorGetQuery__
 *
 * To run a query within a React component, call `useDoorGetQuery` and pass it any options that fit your needs.
 * When your component renders, `useDoorGetQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDoorGetQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDoorGetQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<DoorGetQuery, DoorGetQueryVariables>) {
        return ApolloReactHooks.useQuery<DoorGetQuery, DoorGetQueryVariables>(DoorGetDocument, baseOptions);
      }
export function useDoorGetLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<DoorGetQuery, DoorGetQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<DoorGetQuery, DoorGetQueryVariables>(DoorGetDocument, baseOptions);
        }
export type DoorGetQueryHookResult = ReturnType<typeof useDoorGetQuery>;
export type DoorGetLazyQueryHookResult = ReturnType<typeof useDoorGetLazyQuery>;
export const DoorAcessPermissionDocument = gql`
    query DoorAcessPermission($id: ID!) {
  doorAcessPermissiion(id: $id) {
    acess
    doorId
    nome
    resultMsg
    userRfId
  }
}
    `;

/**
 * __useDoorAcessPermissionQuery__
 *
 * To run a query within a React component, call `useDoorAcessPermissionQuery` and pass it any options that fit your needs.
 * When your component renders, `useDoorAcessPermissionQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDoorAcessPermissionQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDoorAcessPermissionQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<DoorAcessPermissionQuery, DoorAcessPermissionQueryVariables>) {
        return ApolloReactHooks.useQuery<DoorAcessPermissionQuery, DoorAcessPermissionQueryVariables>(DoorAcessPermissionDocument, baseOptions);
      }
export function useDoorAcessPermissionLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<DoorAcessPermissionQuery, DoorAcessPermissionQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<DoorAcessPermissionQuery, DoorAcessPermissionQueryVariables>(DoorAcessPermissionDocument, baseOptions);
        }
export type DoorAcessPermissionQueryHookResult = ReturnType<typeof useDoorAcessPermissionQuery>;
export type DoorAcessPermissionLazyQueryHookResult = ReturnType<typeof useDoorAcessPermissionLazyQuery>;
export const DoorGrantAcessDocument = gql`
    query DoorGrantAcess($id: ID!, $userRfId: String, $reason: String) {
  doorGrantAcess(id: $id, userRfId: $userRfId, reason: $reason) {
    acess
    doorId
    nome
    resultMsg
    userRfId
    motivo
  }
}
    `;

/**
 * __useDoorGrantAcessQuery__
 *
 * To run a query within a React component, call `useDoorGrantAcessQuery` and pass it any options that fit your needs.
 * When your component renders, `useDoorGrantAcessQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDoorGrantAcessQuery({
 *   variables: {
 *      id: // value for 'id'
 *      userRfId: // value for 'userRfId'
 *      reason: // value for 'reason'
 *   },
 * });
 */
export function useDoorGrantAcessQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<DoorGrantAcessQuery, DoorGrantAcessQueryVariables>) {
        return ApolloReactHooks.useQuery<DoorGrantAcessQuery, DoorGrantAcessQueryVariables>(DoorGrantAcessDocument, baseOptions);
      }
export function useDoorGrantAcessLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<DoorGrantAcessQuery, DoorGrantAcessQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<DoorGrantAcessQuery, DoorGrantAcessQueryVariables>(DoorGrantAcessDocument, baseOptions);
        }
export type DoorGrantAcessQueryHookResult = ReturnType<typeof useDoorGrantAcessQuery>;
export type DoorGrantAcessLazyQueryHookResult = ReturnType<typeof useDoorGrantAcessLazyQuery>;