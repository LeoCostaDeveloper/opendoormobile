import ApolloClient from 'apollo-boost'
import { DOORS_GETALL_QUERY } from './graphql/doors'

const uri = process.env.REACT_APP_API_URL

const client = new ApolloClient({
  uri,
})

client
  .query({
    query: DOORS_GETALL_QUERY,
  })
  .then(result => console.log('doors: ', result))
  .catch(err => console.log('erro:', err))

export { client }
